require 'httpclient'

class ApiSuggest
  def self.suggest(keyword, max_num)
    url = ENV['URL']
    query = { keyword: keyword, max_num: max_num }
    headers = { Authorization: "Bearer #{ENV['API_KEY']}" }
    HTTPClient.get(url, query, headers)
  end
end
