class Potepan::SuggestsController < ApplicationController
  def search
    if params[:keyword].blank?
      logger.error("keyword is blank")
      return render status: 400, json: { message: "keyword is blank" }
    end

    response = ApiSuggest.suggest(params[:keyword], params[:max_num])

    if response.status == 200
      render json: JSON.parse(response.body)
    else
      logger.error("Request error status: #{response.status}")
      render status: response.status, json: { message: "Request error" }
    end
  end
end
