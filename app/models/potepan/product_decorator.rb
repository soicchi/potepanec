module Potepan::ProductDecorator
  def related_products
    Spree::Product.in_taxons(taxons).distinct.
      where.not(id: id).includes(master: [:images, :default_price])
  end

  Spree::Product.prepend self
end
