require 'rails_helper'

RSpec.describe "Product Model", type: :model do
  let(:taxon1) { create(:taxon) }
  let(:taxon2) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon1]) }
  let!(:related_product) { create(:product, taxons: [taxon1]) }
  let!(:not_related_product) { create(:product, taxons: [taxon2]) }

  describe "Valid related product" do
    it "Product taxon should be containing related product taxon" do
      expect(product.related_products).to include related_product
    end

    it "Product taxon should not be containing not related product taxon" do
      expect(product.related_products).not_to include not_related_product
    end

    it "Main product should not be present in related product list" do
      expect(product.related_products).not_to include product
    end
  end
end
