require 'rails_helper'

RSpec.describe "Suggests Spec", type: :request do
  before do
    WebMock.enable!
  end

  let(:keyword) { "r" }
  let(:max_num) { 5 }
  let(:success_status) { 200 }
  let(:body) { ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json }
  let(:blank_keyword) { " " }
  let(:error_status) { 400 }

  describe "/GET Api" do
    context "keyword is not blank" do
      before do
        webmock_method(keyword, max_num, success_status)
        get potepan_suggests_path, params: { keyword: keyword, max_num: max_num }
      end

      it "return to success" do
        expect(response.status).to eq(200)
      end

      it "return correct content" do
        expect(JSON.parse(response.body)).to eq(["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"])
      end

      it "correct product name should be 5 displays" do
        expect(JSON.parse(response.body).size).to eq 5
      end
    end

    context "keyword is blank" do
      before do
        webmock_method(blank_keyword, max_num, error_status)
        get potepan_suggests_path, params: { keyword: blank_keyword, max_num: max_num }
      end

      it "return status 400" do
        expect(response.status).to eq 400
      end
    end
  end
end
