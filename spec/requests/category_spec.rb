require 'rails_helper'

RSpec.describe "Category Request", type: :request do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }

  describe "GET /show" do
    before do
      get potepan_category_path(taxon.id)
    end

    it "return http success" do
      expect(response).to have_http_status(:success)
    end

    it "return show template" do
      expect(response).to render_template(:show)
    end

    it "Correct category name should be present" do
      expect(response.body).to include taxon.name
    end
  end
end
