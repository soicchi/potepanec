require 'rails_helper'

RSpec.describe "Products Request", type: :request do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon)   { create(:taxon) }

  describe "GET /show" do
    before do
      get potepan_product_path(product.id)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns show template" do
      expect(response).to render_template(:show)
    end

    it "Correct product name should be present" do
      expect(response.body).to include product.name
    end

    it "Correct product price should be present" do
      expect(response.body).to include product.display_price.to_s
    end

    it "Correct product description should be present" do
      expect(response.body).to include product.description
    end
  end
end
