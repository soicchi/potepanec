require 'rails_helper'

RSpec.describe "Sample Request", type: :request do
  describe "GET /index" do
    before do
      get potepan_path
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns index template" do
      expect(response).to render_template(:index)
    end
  end
end
