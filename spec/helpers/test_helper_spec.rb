require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  let(:base_title) { "BIGBAG Store" }

  describe "correct title" do
    subject { helper.full_title(title) }

    context "page_title is blank" do
      let(:title) { "" }

      it { is_expected.to eq(base_title) }
    end

    context "page_title is nil" do
      let(:title) { nil }

      it { is_expected.to eq(base_title) }
    end

    context "page_title is not blank" do
      let(:title) { "example_title" }

      it { is_expected.to eq("example_title - #{base_title}") }
    end
  end
end
