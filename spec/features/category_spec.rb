require 'rails_helper'

RSpec.feature "Category Features", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, name: "Example item", taxons: [taxon]) }
  let(:base_title) { "BIGBAG Store" }

  describe "GET potepan_category_path" do
    before do
      visit potepan_category_path(taxon.id)
    end

    it "Correct title should be present" do
      expect(page).to have_title "#{taxon.name} - #{base_title}"
    end

    it "Correct taxon name should be present" do
      expect(page).to have_selector 'h2', text: taxon.name
    end

    it "Home link should be correct" do
      within ".breadcrumb" do
        click_on "Home"
      end
      visit potepan_path
      expect(current_path).to eq potepan_path
    end

    it "Category link should be correct with clicking category name" do
      within ".side-nav" do
        expect(page).to have_content taxonomy.name
        click_on taxonomy.name, match: :first
        expect(page).to have_content taxon.name
        click_on taxon.name, match: :first
      end
      visit potepan_category_path(taxon.id)
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it "Product link should be present" do
      expect(page).to have_content product.name
      expect(page).to have_link product.name
    end

    it "Product link should be correct" do
      click_on product.name
      visit potepan_product_path(product.id)
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
