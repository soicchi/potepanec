require 'rails_helper'

RSpec.feature "Products Features", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon) { create(:taxon) }
  let(:taxon2) { create(:taxon) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let!(:not_related_product) { create(:product, taxons: [taxon2]) }
  let(:base_title) { "BIGBAG Store" }

  describe "GET /show" do
    before do
      visit potepan_product_path(product.id)
    end

    it "Correct titile should be present" do
      expect(page).to have_title "#{product.name} - #{base_title}"
    end

    it "Correct link should be present" do
      click_on '一覧ページへ戻る'
      visit potepan_category_path(taxon.id)
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it "Correct product name should be present" do
      expect(page).to have_selector 'h2', text: product.name
    end

    it "Correct product price should be present" do
      expect(page).to have_selector 'h3', text: product.display_price
    end

    it "Correct product description should be present" do
      expect(page).to have_selector 'p', text: product.description
    end

    it "Related product name should be 4 displays" do
      expect(page).to have_content related_products.first.name
      expect(page).to have_content related_products.second.name
      expect(page).to have_content related_products.third.name
      expect(page).to have_content related_products.fourth.name
    end

    it "Related product link should be 4 displays" do
      expect(page).to have_link related_products.first.name
      expect(page).to have_link related_products.second.name
      expect(page).to have_link related_products.third.name
      expect(page).to have_link related_products.fourth.name
    end

    it "Related product link should be correct" do
      click_on related_products.first.name
      visit potepan_product_path(related_products.first.id)
      expect(current_path).to eq potepan_product_path(related_products.first.id)
    end

    it "Related product count should be 4" do
      within ".productsContent" do
        expect(page).to have_selector ".productBox", count: 4
      end
    end

    it "Not related products should not display" do
      expect(page).not_to have_content not_related_product.name
      expect(page).not_to have_link not_related_product.name
    end

    it "Main product in product pages should not display" do
      expect(page).not_to have_link product.name
    end
  end
end
