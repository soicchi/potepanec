module SuggestWebmock
  def webmock_method(keyword, max_num, status)
    WebMock.stub_request(:get, ENV['URL']).with(
      headers: { Authorization: "Bearer #{ENV['API_KEY']}" },
      query: { keyword: keyword, max_num: max_num },
    ).to_return(
      body: body,
      status: status,
      headers: { "Content-Type" => "application/json" }
    )
  end
end
